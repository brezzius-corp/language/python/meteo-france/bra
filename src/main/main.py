#!/usr/bin/env python3

"""Archive BRA from Meteo-France"""

import http.client as chttp
import datetime as dt
import os
import sys
import json

HOSTNAME1 = 'donneespubliques.meteofrance.fr'
HOSTNAME2 = 'api.meteofrance.com'
PREFIXE1 = '/donnees_libres/Pdf/BRA/'
PREFIXE2 = '/files/mountain/bulletins/'
OUTPUT = '.'

def archivage(cdate1, cdate2):
    """Archive data for specified date
       Arguments:
         cdate1 : date to archive (previous day) for PDF
         cdate2 : date to archive (current day) for XML
       Return: None
    """

    print("Archivage du " + cdate1)

    # Archive pdf files

    path = PREFIXE1 + 'bra.' + cdate1 + '.json'

    cnx = chttp.HTTPSConnection(HOSTNAME1)
    cnx.request('GET', path)

    response = cnx.getresponse()
    data = response.read()

    if response.status != 200 or response.reason != 'OK':
        while 1:
            cnx.close()
            cnx = chttp.HTTPSConnection(HOSTNAME1)
            cnx.request('GET', path)
            response = cnx.getresponse()
            data = response.read()
            if response.status == 200 and response.reason == 'OK':
                break

    cnx.close()

    parsed_json = json.loads(data)

    for item in parsed_json:
        path = PREFIXE1 + 'BRA.' + item['massif'] + '.' + item['heures'][0] + '.pdf'

        cnx = chttp.HTTPSConnection(HOSTNAME1)
        cnx.request('GET', path)

        response = cnx.getresponse()
        data = response.read()

        if response.status != 200 or response.reason != 'OK':
            while 1:
                cnx.close()
                cnx = chttp.HTTPSConnection(HOSTNAME1)
                cnx.request('GET', path)
                response = cnx.getresponse()
                data = response.read()
                if response.status == 200 and response.reason == 'OK' or response.status == 302:
                    break

        cnx.close()

        try:
            if not os.path.exists(OUTPUT + 'pdf/' + item['massif']):
                os.makedirs(OUTPUT + 'pdf/' + item['massif'])

            fname = cdate1[:4] + '_' + cdate1[4:6] + '_' + cdate1[6:8] + '.pdf'
            dfile = open(OUTPUT + 'pdf/' + item['massif'] + '/' + fname, 'wb')
            dfile.write(data)
            dfile.close()

        except KeyError:
            print(item['massif'], item['heures'])
            continue

    # Archive XML files

    if cdate2 is not None:
        massif = {'01': 'CHABLAIS', '02': 'ARAVIS', '03': 'MONT-BLANC', '04': 'BAUGES', '05': 'BEAUFORTAIN',
                  '06': 'HAUTE-TARENTAISE', '07': 'CHARTREUSE', '08': 'BELLEDONNE', '09': 'MAURIENNE', '10': 'VANOISE',
                  '11': 'HAUTE-MAURIENNE', '12': 'GRANDES-ROUSSES', '13': 'THABOR', '14': 'VERCORS', '15': 'OISANS',
                  '16': 'PELVOUX', '17': 'QUEYRAS', '18': 'DEVOLUY', '19': 'CHAMPSAUR', '20': 'EMBRUNAIS-PARPAILLON',
                  '21': 'UBAYE', '22': 'HAUT-VAR-HAUT-VERDON', '23': 'MERCANTOUR', '40': 'CINTO-ROTONDO', '41': 'RENOSO-INCUDINE',
                  '64': 'PAYS-BASQUE', '65': 'ASPE-OSSAU', '66': 'HAUTE-BIGORRE', '67': 'AURE-LOURON', '68': 'LUCHONNAIS',
                  '69': 'COUSERANS', '70': 'HAUTE-ARIEGE', '71': 'ANDORRE', '72': 'ORLU-ST_BARTHELEMY', '73': 'CAPCIR-PUYMORENS',
                  '74': 'CERDAGNE-CANIGOU'}

        for key, value in massif.items():
            path = PREFIXE2 + 'BRA' + key + '.xml'

            cnx = chttp.HTTPSConnection(HOSTNAME2)
            cnx.request('GET', path)

            response = cnx.getresponse()
            data = response.read()

            if response.status == 200 and response.reason == 'OK':
                if not os.path.exists(OUTPUT + 'xml/' + value):
                    os.makedirs(OUTPUT + 'xml/' + value)

                fname = cdate2[:4] + '_' + cdate2[4:6] + '_' + cdate2[6:8] + '.xml'
                dfile = open(OUTPUT + 'xml/' + value + '/' + fname, 'wb')
                dfile.write(data)
                dfile.close()

        cnx.close()


def leapyear(year):
    """Leap year
       Arguments:
         year : year to test
       Return: True if leap year, else false
    """
    return year % 4 == 0 and year % 100 != 0 or year % 400 == 0


def main():
    """Main function"""

    today = dt.datetime.now()

    if len(sys.argv) > 1 and sys.argv[1] == '--full':
        for i in range(2016, today.year + 1):
            for j in range(1, 13):
                for k in range(1, 32):
                    if i == today.year and j == today.month and k == today.day + 1:
                        sys.exit(0)

                    if ((j in (4, 6, 9, 11)) and (k == 31)):
                        break

                    if (j == 2) and ((k == 29) or (leapyear(i) and (k == 30))):
                        break

                    date = '%04d%02d%02d' %(i, j, k)
                    name = PREFIXE1 + 'bra.' + date  + '.json'

                    cnx = chttp.HTTPSConnection(HOSTNAME1)
                    cnx.request("GET", name)

                    response = cnx.getresponse()
                    response.read()

                    if response.status == 200 and response.reason == 'OK':
                        archivage(date, None)

                    cnx.close()
    else:
        cdate1 = (dt.datetime.today() - dt.timedelta(days=1)).strftime('%Y%m%d')
        cdate2 = (dt.datetime.today()).strftime('%Y%m%d')
        archivage(cdate1, cdate2)


if __name__ == "__main__":
    main()
